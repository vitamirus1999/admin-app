<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('payments', function (Blueprint $table) {
			$table->id('payment_id');
			$table->string('user_id', 100);
			$table->integer('payment_status');
			$table->integer('app_type');
			$table->string('os_type', 10);
			$table->float('cost');
			$table->integer('currency_id');
			$table->ipAddress('ip');
			$table->dateTimeTz('dt_init')->default('1900-01-01 00:00:00');
			$table->dateTimeTz('dt_complete')->default('1900-01-01 00:00:00');;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('payments');
    }
};
