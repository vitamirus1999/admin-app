<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->id('log_id');
			$table->string('user_id', 100);
			$table->string('end_point', 100);
			$table->integer('action_id');
			$table->integer('app_type');
			$table->string('os_type', 10);
			$table->ipAddress('ip');
			$table->dateTimeTz('created_at');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('logs');
    }
};
