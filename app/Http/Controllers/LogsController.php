<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLogsRequest;
use App\Http\Requests\UpdateLogsRequest;
use App\Models\Log;

class LogsController extends Controller
{
	/** Объект модели лога */
	private Log $logs;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreLogsRequest $request)
    {
		$this->logs = new Log();
		$this->logs->add($request->all());

        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Log $logs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Log $logs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateLogsRequest $request, Log $logs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Log $logs)
    {
        //
    }
}
