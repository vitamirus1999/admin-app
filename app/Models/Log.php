<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;

	public $timestamps = false;

	public function add(array $request): bool
	{
		$this->user_id = $request['user_id'];
		$this->end_point = $request['end_point'];
		$this->action_id = $request['action_id'];
		$this->app_type = $request['app_type'];
		$this->os_type = $request['os_type'];
		$this->ip = $request['ip'];
		$this->created_at = date('Y-m-d H:i:s');
		return $this->save();
	}
}
