<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

	public $timestamps = false;

	public function add(array $request): bool
	{
		$this->user_id = $request['user_id'];
		$this->payment_status = 0;
		$this->app_type = $request['app_type'];
		$this->os_type = $request['os_type'];
		$this->cost = $request['cost'];
		$this->currency_id = $request['currency_id'];
		$this->ip = $request['ip'];
		$this->dt_init = date('Y-m-d H:i:s');
		return $this->save();
	}

//	public function update(array $request): bool
//	{
//
//	}
}
